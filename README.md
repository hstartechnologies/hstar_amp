# AMP Driver Package

## System Files

System files are stored in the system_files directory. This should include udev rules, a systemd service, and a bash script for the systemd service to call. The udev rules belong in /etc/udev/rules.d/ and the systemd file belongs in /etc/systemd/system/.

## Firmware

Instructions for building the firmware are included in their respective packages.

# ROS API

## mecanum_base.py

Provides basic interfacing to the motor controller and reports hall counts. Implicit system information (such as device names) are hardcoded.

### Published Topics

~hall_count/<controller_name> (std_msgs/Int32)

	Raw hall counts (accumulating) as reported by the motor controller

### Subscribed Topics

~command (geometry_msgs/Vector3)

	Command vector. Clamped to [-1.0,1.0]. Open loop current command. Input value represents a precentage of (electrical) current output.

~twist_command (geometry_msgs/Twist)

	Command vector in the format of a Twist. Only linear x,y and angular z used. Same limits/functionality as ~command.

/joy (sensor_msgs/Joy)

	Joystick command as provided by joy_node. Left/right joysticks used for driving. Limits speed to 1/3 normally, but full speed when either shoulder button is pressed.

## mecanum_odometry.py

Provides a simple wheel odometry estimate based on hall count information provided by mecanum_base.py

### Parameters

~base_frame (string, default:"base_link")

~odom_frame (string, default:"odom")

~publish_tf (bool, default:true)

~loop_rate (int, default:100)

### Published Topics

pose (geometry_msgs/PoseStamped)

twist (geometry_msgs/TwistStamped)

joint_states (sensor_msgs/JointState)

odom (nav_msgs/Odometry)

### Subscribed Topics

hall_count/<controller_name> (std_msgs/Int32)

### Transforms Provided

odom -> base_link

## lcd_panel.py

Basic driver for the MCU attached to the LCD panel on the side of the robot. Also reports the battery voltage

### Published Topics

battery_voltage (std_msgs/Float32)

### Subscribed Topics 

~display (std_msgs/String)

	Write something to the bottom line of the LCD. Max 16 characters.
