#Build#

Download and unpack the [STM32CubeL4](http://www.st.com/content/st_com/en/products/embedded-software/mcus-embedded-software/stm32-embedded-software/stm32cube-embedded-software/stm32cubeL4.html) package and point to it in the Makefile. (makefile default is ~/cube)

Install the gcc arm cross-compiler either from [launchpad](https://launchpad.net/gcc-arm-embedded/+download). Or on Ubuntu/Debian:

    sudo apt-get install gcc-arm-none-eabi

Program the board using the drag-and-drop option (just copy-paste the .bin file into the virtual drive). Alternatively, use [stlink](https://github.com/texane/stlink). (make install)
