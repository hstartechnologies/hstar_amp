#ifndef __ADC_H__
#define __ADC_H__
#include "global.h"

#define ADC_CONVERTED_DATA_BUFFER_SIZE 1

extern uint16_t g_adc_converted_data[ADC_CONVERTED_DATA_BUFFER_SIZE];
extern float g_adc_adjusted_data[ADC_CONVERTED_DATA_BUFFER_SIZE];
extern float g_adc_filtered_data[ADC_CONVERTED_DATA_BUFFER_SIZE];
extern ADC_HandleTypeDef g_adc_handler;
extern const int16_t ADC_OFFSET[];
extern const float ADC_ADJUSTMENT[];


void adc_init(void);

#endif /*__ADC_H__*/
