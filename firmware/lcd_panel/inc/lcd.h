
#ifndef __USART_LCD_H__
#define __USART_LCD_H__

#include "stm32l4xx_hal.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_usart.h"
#include <stdarg.h> 
#define PRINTF_BUFFER_SIZE 128
#define TX_BUFFER_SIZE 128

void lcd_init(void);
void lcd_putc(uint8_t c);
void lcd_printf(const char* format, ...);
void lcd_txe_int_handler();
void lcd_error_int_handler();

#endif /*__USART_LCD_H__*/