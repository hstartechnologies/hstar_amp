#include "adc.h"

uint16_t g_adc_converted_data[ADC_CONVERTED_DATA_BUFFER_SIZE];
float g_adc_filtered_data[ADC_CONVERTED_DATA_BUFFER_SIZE];
ADC_HandleTypeDef g_adc_handler;

void HAL_ADC_MspInit(ADC_HandleTypeDef *hadc)
{
  static DMA_HandleTypeDef         DmaHandle;
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_ADC_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();
  //__HAL_RCC_ADC_CONFIG(RCC_ADCCLKSOURCE_SYSCLK);
  
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


  DmaHandle.Instance                 = DMA1_Channel1;
  DmaHandle.Init.Direction           = DMA_PERIPH_TO_MEMORY;
  DmaHandle.Init.PeriphInc           = DMA_PINC_DISABLE;
  DmaHandle.Init.MemInc              = DMA_MINC_ENABLE;
  DmaHandle.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
  DmaHandle.Init.MemDataAlignment    = DMA_MDATAALIGN_HALFWORD;
  DmaHandle.Init.Mode                = DMA_CIRCULAR;
  DmaHandle.Init.Priority            = DMA_PRIORITY_MEDIUM;
  /* Deinitialize  & Initialize the DMA for new transfer */
  HAL_DMA_DeInit(&DmaHandle);
  HAL_DMA_Init(&DmaHandle);
  __HAL_LINKDMA(hadc, DMA_Handle, DmaHandle);
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 7, 15);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  HAL_NVIC_SetPriority(ADC1_2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(ADC1_2_IRQn);
}

void adc_init(void){
    for(int i = 0; i < ADC_CONVERTED_DATA_BUFFER_SIZE; i++){
        g_adc_converted_data[i] = 0;
        g_adc_filtered_data[i] = 0.0;
    }

	g_adc_handler.Instance          = ADC1;
	g_adc_handler.Init.ClockPrescaler        = ADC_CLOCK_SYNC_PCLK_DIV4;      /* Synchronous clock mode, input ADC clock divided by 2*/
	g_adc_handler.Init.Resolution            = ADC_RESOLUTION_12B;            /* 12-bit resolution for converted data */
	g_adc_handler.Init.DataAlign             = ADC_DATAALIGN_RIGHT;           /* Right-alignment for converted data */
	g_adc_handler.Init.ScanConvMode          = ENABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
	g_adc_handler.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;           /* EOC flag picked-up to indicate conversion end */
	g_adc_handler.Init.LowPowerAutoWait      = DISABLE;                       /* Auto-delayed conversion feature disabled */
	g_adc_handler.Init.ContinuousConvMode    = ENABLE;                        /* Continuous mode enabled (automatic conversion restart after each conversion) */
	g_adc_handler.Init.NbrOfConversion       = 1;                             /* Parameter discarded because sequencer is disabled */
	g_adc_handler.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
	g_adc_handler.Init.NbrOfDiscConversion   = 1;                             /* Parameter discarded because sequencer is disabled */
	g_adc_handler.Init.ExternalTrigConv      = ADC_SOFTWARE_START;            /* Software start to trig the 1st conversion manually, without external event */
	g_adc_handler.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE; /* Parameter discarded because software trigger chosen */
	g_adc_handler.Init.DMAContinuousRequests = ENABLE;                        /* ADC DMA continuous request to match with DMA circular mode */
	g_adc_handler.Init.Overrun               = ADC_OVR_DATA_OVERWRITTEN;      /* DR register is overwritten with the last conversion result in case of overrun */
    g_adc_handler.Init.OversamplingMode      = DISABLE;                       /* No oversampling */
	ADC_ChannelConfTypeDef channel_config;
	HAL_ADC_Init(&g_adc_handler);
	HAL_ADCEx_Calibration_Start(&g_adc_handler, ADC_SINGLE_ENDED);
	channel_config.Channel      = ADC_CHANNEL_8;               /* Sampled channel number */
	channel_config.Rank         = ADC_REGULAR_RANK_1;          /* Rank of sampled channel number ADCx_CHANNEL */
	channel_config.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;   /* Sampling time (number of clock cycles unit) */
	channel_config.SingleDiff   = ADC_SINGLE_ENDED;            /* Single-ended input channel */
	channel_config.OffsetNumber = ADC_OFFSET_NONE;             /* No offset subtraction */ 
	channel_config.Offset = 0;                                 /* Parameter discarded because offset correction is disabled */
	HAL_ADC_ConfigChannel(&g_adc_handler, &channel_config);


	HAL_ADCEx_Calibration_Start(&g_adc_handler, ADC_SINGLE_ENDED);

	HAL_ADC_Start_DMA(&g_adc_handler, (uint32_t *)g_adc_converted_data, ADC_CONVERTED_DATA_BUFFER_SIZE );
}

