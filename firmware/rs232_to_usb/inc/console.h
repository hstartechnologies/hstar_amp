
#ifndef __USART_CONSOLE_H__
#define __USART_CONSOLE_H__

#include "stm32l4xx_hal.h"
#include "stm32l4xx_ll_gpio.h"
#include "stm32l4xx_ll_usart.h"
#include <stdarg.h> 
#define PRINTF_BUFFER_SIZE 128
#define TX_BUFFER_SIZE 128
#define RX_BUFFER_SIZE 128

void console_init(void);
void console_putc(uint8_t c);
int console_async_getc(uint8_t *c);
uint8_t console_sync_getc();
void console_printf(const char* format, ...);
void console_txe_int_handler();
void console_rxne_int_handler();
void console_error_int_handler();

#endif /*__USART_CONSOLE_H__*/