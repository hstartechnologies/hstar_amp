
#include "global.h"
#include "stm32l4xx_it.h"

void NMI_Handler(void){}
void HardFault_Handler(void) { HAL_NVIC_SystemReset(); }
void MemManage_Handler(void) { HAL_NVIC_SystemReset(); }
void BusFault_Handler(void) { HAL_NVIC_SystemReset(); }
void UsageFault_Handler(void) { HAL_NVIC_SystemReset(); }
void SVC_Handler(void){}
void DebugMon_Handler(void){}
void PendSV_Handler(void){}
void SysTick_Handler(void){ HAL_IncTick(); }

void TIM2_IRQHandler(void)
{
    if(LL_TIM_IsActiveFlag_CC1(TIM2))
    {
        LL_TIM_ClearFlag_CC1(TIM2);
        led_toggle(); // toggle onboard LED at 1hz
    }
}

void USART2_IRQHandler(void)
{
    if(LL_USART_IsActiveFlag_RXNE(USART2) && LL_USART_IsEnabledIT_RXNE(USART2))
    {
        console_rxne_int_handler();
        uint8_t c;
        while(console_async_getc(&c)) roboteq_putc(c);
    }
    if(LL_USART_IsActiveFlag_TXE(USART2) && LL_USART_IsEnabledIT_TXE(USART2)){
        console_txe_int_handler();
    }
    if((LL_USART_IsActiveFlag_FE(USART2) || LL_USART_IsActiveFlag_NE(USART2) || LL_USART_IsActiveFlag_ORE(USART2)) && LL_USART_IsEnabledIT_ERROR(USART2)){
        console_error_int_handler();
    }
}

void USART1_IRQHandler(void)
{
    if(LL_USART_IsActiveFlag_RXNE(USART1) && LL_USART_IsEnabledIT_RXNE(USART1))
    {
        roboteq_rxne_int_handler();
        uint8_t c;
        while(roboteq_async_getc(&c)) console_putc(c);
    }
    if(LL_USART_IsActiveFlag_TXE(USART1) && LL_USART_IsEnabledIT_TXE(USART1)){
        roboteq_txe_int_handler();
    }
    if((LL_USART_IsActiveFlag_FE(USART1) || LL_USART_IsActiveFlag_NE(USART1) || LL_USART_IsActiveFlag_ORE(USART1)) && LL_USART_IsEnabledIT_ERROR(USART1)){
        roboteq_error_int_handler();
    }
}
