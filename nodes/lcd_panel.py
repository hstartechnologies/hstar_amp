#!/usr/bin/env python
import struct, rospy, roslib
from std_msgs.msg import Float32,String
from serial import Serial, SerialException
from threading import Thread
packet_format = '<BfB'

if __name__ == '__main__':
    try:
        rospy.init_node('lcd_panel')
        port = rospy.get_param('~port', "/dev/ros/lcd")
        baud = rospy.get_param('~baud_rate',115200)
        while not rospy.is_shutdown():
            try:
                with Serial(port,baud) as pipe:
                    def read_thread(pipe):
                        pub = rospy.Publisher('battery_voltage',Float32, queue_size=1)
                        line = str()
                        while not rospy.is_shutdown():
                            this = pipe.read(1)
                            line += this
                            if line[0] != chr(0x7E):
                                line = str()
                            elif len(line) == struct.calcsize(packet_format):
                                (start, voltage, end) = packet = struct.unpack(packet_format, line)
                                if end == 0x81:
                                    pub.publish(Float32(voltage))
                                line = str()
                    thread = Thread(target= lambda a=pipe: read_thread(a))
                    thread.daemon = True
                    thread.start()
                    pipe.write(chr(0xFE)+chr(192)+'Status: Ready   ')
                    def display_cb(string):
                        pipe.write(chr(0xFE)+chr(192)+string.data[:16]+' '*(16-len(string.data)))
                    rospy.Subscriber("~display",String,display_cb)
                    rospy.spin()
            except SerialException as e:
                rospy.logerr(str(e))
                rospy.sleep(1.0)
    except rospy.ROSInterruptException:
        pass
