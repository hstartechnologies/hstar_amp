#!/usr/bin/env python
import rospy, roslib
from serial import Serial, SerialException
from threading import Thread
roslib.load_manifest("geometry_msgs")
roslib.load_manifest("std_msgs")
roslib.load_manifest("sensor_msgs")
from geometry_msgs.msg import Twist,Vector3
from std_msgs.msg import Int32
from sensor_msgs.msg import Joy

names = ("front_left", "front_right", "back_left", "back_right")
invert = (True,False,True,False)
roboteq_devices = ["/dev/ros/roboteq/"+name for name in names]
timeout = 1.0

def clamp(val, limit):
    if val > limit: return limit
    elif val < -limit: return -limit
    else: return val

def twist_to_mecanum(twist, clamped=True):
    front_left  = twist.linear.x - twist.linear.y - twist.angular.z
    front_right = twist.linear.x + twist.linear.y + twist.angular.z
    back_left   = twist.linear.x + twist.linear.y - twist.angular.z
    back_right  = twist.linear.x - twist.linear.y + twist.angular.z
    setpoints = [-s if i else s for s,i in zip([front_left, front_right, back_left, back_right],invert)]
    if clamped:
        return [clamp(x,1.0) for x in setpoints]
    else:
        return setpoints

def vec3_to_mecanum(vec, clamped=True):
    front_left  = vec.x - vec.y - vec.z
    front_right = vec.x + vec.y + vec.z
    back_left   = vec.x + vec.y - vec.z
    back_right  = vec.x - vec.y + vec.z
    setpoints = [-s if i else s for s,i in zip([front_left, front_right, back_left, back_right],invert)]
    if clamped:
        return [clamp(x,1.0) for x in setpoints]
    else:
        return setpoints

class Roboteq(Thread):
    def __init__(self, name, device, invert=False):
        super(Roboteq, self).__init__()
        self.name = name
        self.device = device
        self.invert = invert
        self.invert_mult = 1 if invert else -1
        self.daemon = True
        self.pub = rospy.Publisher('~hall_count/'+name,Int32,queue_size=3)
    def run(self):
        while not rospy.is_shutdown():
            try:
                self.pipe = Serial(self.device,115200,timeout=timeout)
                self.pipe.write('!CB 0\r\n') # clear the hall count
                self.pipe.write('# C\r\n') # clear history
                self.pipe.write('?CB\r\n') # request hall count
                self.pipe.write('# 10\r\n') # repeat every 10 ms = 100 hz
                line = ''
                while not rospy.is_shutdown():
                    line += self.pipe.read()
                    if len(line) > 0:
                        if line[-1] == '\r':
                            if line[0:3] == 'CB=':
                                try:
                                    val = self.invert_mult*int(line[3:].rstrip('\r\n'))
                                    self.pub.publish(Int32(val))
                                except ValueError:
                                    pass
                            line = ''
            except SerialException:
                rospy.logerr("[Roboteq] "+self.name+" broken pipe. Attempting to reconnect")
                rospy.sleep(1.0)
            finally:
                try:
                    self.pipe.close()
                except:
                    pass


if __name__ == '__main__':
    try:
        rospy.init_node('mecanum_base')
        roboteqs = [Roboteq(n,r,i) for n,r,i in zip(names,roboteq_devices,invert)]
        for roboteq in roboteqs:
            roboteq.start()
        def twist_cb(twist):
            setpoints = twist_to_mecanum(twist)
            for roboteq,setpoint in zip(roboteqs,setpoints):
                msg = '!G %d\r\n'%clamp(int(setpoint*1000),1000)
                try:
                    roboteq.pipe.write(msg)
                except SerialException,AttributeError:
                    pass
        rospy.Subscriber('~twist_command', Twist, twist_cb)
        def vec_cb(vec):
            setpoints = vec3_to_mecanum(vec)
            for roboteq,setpoint in zip(roboteqs,setpoints):
                msg = '!G %d\r\n'%clamp(int(setpoint*1000),1000)
                try:
                    roboteq.pipe.write(msg)
                except SerialException,AttributeError:
                    pass
        rospy.Subscriber('~command', Vector3, vec_cb)
        def joy_cb(joy):
            vec = Vector3()
            vec.y, vec.x, _, vec.z = joy.axes[:4]
            left_trig, right_trig = joy.buttons[4:6]
            if not (left_trig or right_trig):
                vec.x /= 3
                vec.y /= 3
                vec.z /= 3
            vec_cb(vec)
        rospy.Subscriber('/joy', Joy, joy_cb)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
