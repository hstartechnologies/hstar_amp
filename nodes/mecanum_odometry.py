#!/usr/bin/env python
import rospy, roslib, tf, numpy
roslib.load_manifest('std_msgs')
roslib.load_manifest('geometry_msgs')
roslib.load_manifest('sensor_msgs')
roslib.load_manifest('nav_msgs')
from std_msgs.msg import Int32
from geometry_msgs.msg import PoseStamped, TwistStamped
from sensor_msgs.msg import JointState
from nav_msgs.msg import Odometry
from math import sin, cos, pi, sqrt
wheel_diameter = 0.2032
wheel_circum = wheel_diameter * pi
center_to_center = 0.578
counts_per_rotation = 40 * 3 * 6 #gearbox * pole pairs * phases
root2 = sqrt(2.)
names =  ["front_left","front_right","back_left","back_right"]
last_count = dict(zip(names,[0]*len(names)))
last_times = dict(zip(names,[0]*len(names)))
last_vel = dict(zip(names,[0]*len(names)))

class LPF:
    def __init__(self, initial_value=0, alpha=.95):
        self.alpha = alpha
        self.value = 0
    def __call__(self, value):
        self.value = (self.alpha * self.value) + ((1.0-self.alpha)*value)
        return self.value

time_lpf = dict(zip(names,[0]*len(names))) # The ROS wall clock is too jittery for small deltas, so a simple lowpass to clean that up

def mecanum_to_twist(fl, fr, bl, br):
    lx = float(fl + fr + bl + br) / 4. / counts_per_rotation * wheel_circum
    ly = float(-fl + fr + bl - br) / 4. / counts_per_rotation * wheel_circum
    az = float(-fl + fr - bl + br) / 4. / counts_per_rotation * wheel_circum / center_to_center / (pi / 2.) * root2
    return lx, ly, az

if __name__ == '__main__':
    try:
        rospy.init_node('mecanum_odom')
        for name in names:
            last_times[name] = rospy.Time.now().to_sec()
            time_lpf[name] = LPF(last_times[name])
        def count_cb(val, name):
            now = rospy.Time.now().to_sec()
            time_diff = time_lpf[name](now - last_times[name])
            last_vel[name] = (val.data - last_count[name]) / time_diff
            last_count[name] = val.data
            last_times[name] = now
        base_frame = rospy.get_param('~base_frame','base_link')
        odom_frame = rospy.get_param('~odom_frame','odom')
        publish_tf = rospy.get_param('~publish_tf',True)
        subs = [rospy.Subscriber('hall_count/'+name, Int32, lambda val,a=name: count_cb(val,a)) for name in names]
        tfbr = tf.TransformBroadcaster()
        pose_pub = rospy.Publisher('pose', PoseStamped, queue_size=1)
        twist_pub = rospy.Publisher('twist', TwistStamped, queue_size=1)
        js_pub = rospy.Publisher('joint_states', JointState, queue_size=1)
        odom_pub = rospy.Publisher('odom', Odometry, queue_size=1)
        rate = rospy.Rate(rospy.get_param('~loop_rate',100))
        twist = TwistStamped()
        twist.header.frame_id = base_frame
        pose = PoseStamped()
        pose.header.frame_id = odom_frame
        pose.pose.orientation.w = 1.
        pose.pose.position.z = wheel_diameter/2
        js = JointState()
        js.header.frame_id = base_frame
        js.name = ['base_to_'+name for name in names]
        odom = Odometry()
        odom.header.frame_id = odom_frame
        odom.child_frame_id = base_frame
        odom.pose.covariance = numpy.eye(6).flatten()*.01
        odom.twist.covariance = numpy.eye(6).flatten()*.01
        last_time = rospy.Time.now()
        theta = 0.0
        while not rospy.is_shutdown():
            now = rospy.Time.now()
            time_diff = now-last_time
            odom.header.stamp = pose.header.stamp = twist.header.stamp = js.header.stamp = now
            js.velocity = [last_vel[name] for name in names]
            js.position = [float(last_count[name])/counts_per_rotation*2*pi for name in names]
            twist.twist.linear.x, twist.twist.linear.y, twist.twist.angular.z = vel = mecanum_to_twist(*[last_vel[name] for name in names])
            x,y,t = [v*time_diff.to_sec() for v in vel]
            pose.pose.position.x += (x*cos(theta)) - (y*sin(theta))
            pose.pose.position.y += (x*sin(theta)) + (y*cos(theta))
            theta += t
            pose.pose.orientation.z = sin(theta/2)
            pose.pose.orientation.w = cos(theta/2)
            if publish_tf:
                tfbr.sendTransform( (pose.pose.position.x,pose.pose.position.y,pose.pose.position.z),
                                    (pose.pose.orientation.x,pose.pose.orientation.y,pose.pose.orientation.z,pose.pose.orientation.w),
                                    now, base_frame, odom_frame)
            odom.pose.covariance += numpy.eye(6).flatten()*.001
            odom.pose.pose = pose.pose
            odom.twist.twist = twist.twist
            twist_pub.publish(twist)
            pose_pub.publish(pose)
            js_pub.publish(js)
            odom_pub.publish(odom)
            last_time = now
            rate.sleep()
    except rospy.ROSInterruptException:
        pass
